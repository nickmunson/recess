//
//  ChoseLocationNewGameViewController.swift
//  Recess
//
//  Created by Nick Munson on 5/1/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class ChooseLocationNewGameViewController: UIViewController {
    
    @IBOutlet weak var a: UIView!
    @IBOutlet weak var b: UIView!
    @IBOutlet weak var c: UIView!
    @IBOutlet weak var d: UIView!
    @IBOutlet weak var e: UIView!
    @IBOutlet weak var f: UIView!
    @IBOutlet weak var g: UIView!
    @IBOutlet weak var h: UIView!
    @IBOutlet weak var i: UIView!
    @IBOutlet weak var j: UIView!
    @IBOutlet weak var k: UIView!
    
    
    @IBOutlet weak var aa: UIButton!
    @IBOutlet weak var bb: UIButton!
    @IBOutlet weak var cc: UIButton!
    @IBOutlet weak var dd: UIButton!
    @IBOutlet weak var ee: UIButton!
    @IBOutlet weak var ff: UIButton!
    @IBOutlet weak var gg: UIButton!
    @IBOutlet weak var hh: UIButton!
    @IBOutlet weak var ii: UIButton!
    @IBOutlet weak var jj: UIButton!
    @IBOutlet weak var kk: UIButton!
    
    
    @IBOutlet weak var backContainer: UIView!
    @IBOutlet weak var backButton: UIButton!
    var game: PFObject!

    override func viewDidLoad() {
        super.viewDidLoad()
        roundCorners()
    }

    func roundCorners() {
        backContainer.layer.cornerRadius = CORNER_RADIUS
        backButton.layer.cornerRadius = CORNER_RADIUS
        a.layer.cornerRadius = CORNER_RADIUS
        b.layer.cornerRadius = CORNER_RADIUS
        c.layer.cornerRadius = CORNER_RADIUS
        d.layer.cornerRadius = CORNER_RADIUS
        e.layer.cornerRadius = CORNER_RADIUS
        f.layer.cornerRadius = CORNER_RADIUS
        g.layer.cornerRadius = CORNER_RADIUS
        h.layer.cornerRadius = CORNER_RADIUS
        i.layer.cornerRadius = CORNER_RADIUS
        j.layer.cornerRadius = CORNER_RADIUS
        k.layer.cornerRadius = CORNER_RADIUS
        aa.layer.cornerRadius = CORNER_RADIUS
        bb.layer.cornerRadius = CORNER_RADIUS
        cc.layer.cornerRadius = CORNER_RADIUS
        dd.layer.cornerRadius = CORNER_RADIUS
        ee.layer.cornerRadius = CORNER_RADIUS
        ff.layer.cornerRadius = CORNER_RADIUS
        gg.layer.cornerRadius = CORNER_RADIUS
        hh.layer.cornerRadius = CORNER_RADIUS
        ii.layer.cornerRadius = CORNER_RADIUS
        jj.layer.cornerRadius = CORNER_RADIUS
        kk.layer.cornerRadius = CORNER_RADIUS

    }
    
    func continueWithLocation(location: String) {
        game[Event.Location] = location
        performSegueWithIdentifier("chooseTimeSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "chooseTimeSegue") {
            (segue.destinationViewController as! ChooseTimeNewGameViewController).game = game
        }
    }
    
    @IBAction func choseWiegand(sender: AnyObject) {
        continueWithLocation(Venue.Wiegand)
    }

    @IBAction func choseSkiboMain(sender: AnyObject) {
        continueWithLocation(Venue.SkiboMain)
    }

    @IBAction func choseSkiboArena(sender: AnyObject) {
        continueWithLocation(Venue.SkiboArena)
    }

    @IBAction func choseSwimmingPool(sender: AnyObject) {
        continueWithLocation(Venue.SwimmingPool)
    }

    @IBAction func choseGeslingTrack(sender: AnyObject) {
        continueWithLocation(Venue.GeslingTrack)
    }

    @IBAction func choseGeslingField(sender: AnyObject) {
        continueWithLocation(Venue.GeslingField)
    }

    @IBAction func choseTenisCourts(sender: AnyObject) {
        continueWithLocation(Venue.TenisCourts)
    }

    @IBAction func choseIntramuralField(sender: AnyObject) {
        continueWithLocation(Venue.IntramuralField)
    }
    
    @IBAction func choseRaquetballCourts(sender: AnyObject) {
        continueWithLocation(Venue.RaquetballCourts)
    }
    
    @IBAction func choseTheCut(sender: AnyObject) {
        continueWithLocation(Venue.TheCut)
    }
    
    @IBAction func choseCFALawn(sender: AnyObject) {
        continueWithLocation(Venue.CFALawn)
    }
    
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
