//
//  GameDetailViewController.swift
//  Recess
//
//  Created by Nick Munson on 5/2/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class GameDetailViewController: UIViewController {

    @IBOutlet weak var attendee1: PFImageView!
    @IBOutlet weak var attendee2: PFImageView!
    @IBOutlet weak var attendee3: PFImageView!
    @IBOutlet weak var attendee4: PFImageView!
    @IBOutlet weak var attendee5: PFImageView!
    
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var creatorImage: PFImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var numPlayersLabel: UILabel!
    @IBOutlet weak var playersAttendingLabel: UILabel!
    @IBOutlet weak var joinButtonContainer: UIView!
    
    @IBOutlet weak var joinContainer: UIView!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var backContainer: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var attendeeScrollView: UIScrollView!
    
    var game: PFObject!
    
    var attendeeImages: Array<PFImageView> = []
    var attendees: Array<PFUser> = []
    var selectedAttendee: Int = -1
    
    enum JoinButton {
        case Join;
        case Leave;
        case None;
    }
    
    var _joinState: JoinButton = JoinButton.None
    var joinState: JoinButton {
        get {
            return _joinState
        }
        set {
            _joinState = newValue
            switch newValue {
            case JoinButton.Join:
                joinButtonContainer.hidden = false
                joinButton.titleLabel?.text = "Join Game"
                joinButton.setNeedsLayout()
                println("join")
            case JoinButton.Leave:
                joinButtonContainer.hidden = false
                joinButton.titleLabel?.text = "Leave Game"
                joinButton.setNeedsLayout()
                println("leave")
            case JoinButton.None:
                joinButtonContainer.hidden = true
                println("none")
            default:
                return
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        attendeeImages.append(attendee1)
//        attendeeImages.append(attendee2)
//        attendeeImages.append(attendee3)
//        attendeeImages.append(attendee4)
//        attendeeImages.append(attendee5)
        roundAttendees()
        loadAttendees()
        loadCreator()
        loadSportImage()
        setJoinButton()
        loadGameInfo()
        roundCorners()
    }
    
    func roundCorners() {
        backContainer.layer.cornerRadius = CORNER_RADIUS
        backButton.layer.cornerRadius = CORNER_RADIUS
        backContainer.layer.cornerRadius = CORNER_RADIUS
        joinContainer.layer.cornerRadius = CORNER_RADIUS
        joinButton.layer.cornerRadius = CORNER_RADIUS
    }
    
    func loadGameInfo() {
        dateLabel.text = Event.formatDate(game)
        timeLabel.text = Event.formatTime(game)
        locationLabel.text = Event.location(game)
        numPlayersLabel.text = Event.numPlayers(game)        
    }
    
    func setJoinButton() {
        let gameDate = game[Event.Time] as! NSDate
        if (gameDate.isEqualToDate(NSDate(timeIntervalSinceNow: NSTimeInterval(-60*60)).earlierDate(gameDate))) {
            self.joinState = JoinButton.None
        } else {
            let userRelation = game.relationForKey(Event.Users)
            userRelation.query()?.findObjectsInBackgroundWithBlock() {
                (objects, error) -> Void in
                if let error = error {
                    println(error)
                } else {
                    if let objects = objects {
                        let me = objects.filter() {
                            (element) -> Bool in
                            let username = (element as! PFObject)[User.Username] as! String
                            let myusername = PFUser.currentUser()?[User.Username] as! String
                            return username == myusername
                        }
                        if (count(me) == 1) {
                            let theirUsername = ((self.game[Event.Creator] as! PFUser)[User.Username]) as! String
                            let myUsername = PFUser.currentUser()?[User.Username] as! String
                            let isMe = (theirUsername == myUsername)
                            if (isMe) {
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.joinState = JoinButton.None
                                })
                            } else {
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.joinState = JoinButton.Leave
                                })
                            }
                        } else {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.joinState = JoinButton.Join
                            })
                        }
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        selectedAttendee = -1
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        creatorImage.layer.cornerRadius = creatorImage.frame.width / 2.0
        creatorImage.layer.zPosition = 100
    }
    
    func loadSportImage() {
        sportImage.image = Sport.image(game["sport"] as! String)
    }
    
    func loadCreator() {
        creatorImage.image = UIImage(contentsOfFile: "Person-icon-grey.jpg")
        let creator = game["creator"] as! PFObject
        creator.fetchIfNeededInBackgroundWithBlock() {
            (success, error) -> Void in
            let creatorPic: PFFile = creator["profilePicture"] as! PFFile
            self.creatorImage.file = creatorPic
            self.creatorImage.loadInBackground()
        }
    }
    
    func roundAttendees() {
        for attendeeImage in attendeeImages {
            attendeeImage.layer.cornerRadius = attendeeImage.frame.width / 2.0
        }
    }
    
    func loadAttendees() {
        let relation = game?.relationForKey(Event.Users)
        relation?.query()?.findObjectsInBackgroundWithBlock() {
            (result, error) -> Void in
            if let attendees = result {
                self.attendees = (attendees as! Array<PFUser>)
                self.displayAttendees()
            }
        }
    }
    
    func displayAttendees() { // track views and remove all if player leaves game TODO
        let numAttendees: Int = count(attendees)
        playersAttendingLabel.text = numAttendees.description
        let length: CGFloat = attendeeScrollView.frame.height
        let space: CGFloat = 8.0
        for image in attendeeImages {
            image.removeFromSuperview()
        }
        for j in 0..<numAttendees {
            let i = j as Int
            let image = PFImageView()
            image.frame = CGRectMake(CGFloat(i) * (length + space), 0.0, length, length)
            image.file = (attendees[i][User.ProfilePicture] as! PFFile)
            image.clipsToBounds = true
            image.layer.cornerRadius = length / 2.0
            let recognizer = UITapGestureRecognizer(target: self, action: "handleTap:")
            recognizer.numberOfTapsRequired = 1
            image.addGestureRecognizer(recognizer)
            image.userInteractionEnabled = true
            attendeeImages.append(image)
            attendeeScrollView.addSubview(image)
        }
        attendeeScrollView.contentSize = CGSizeMake(CGFloat(numAttendees) * (length + space), attendeeScrollView.frame.height)
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        let length: CGFloat = attendeeScrollView.frame.height
        if sender.state == .Ended {
            let point = sender.locationInView(attendeeScrollView)
            selectedAttendee = Int((point.x) / (length + 8.0))
            performSegueWithIdentifier("showOtherProfileSegue", sender: self)
        }
    }
    
    func displayAttendeesStatically() {
        playersAttendingLabel.text = count(attendees).description
        for i in 0..<min(count(attendees), count(attendeeImages)) {
            if let pic: AnyObject = attendees[i][User.ProfilePicture] {
                self.attendeeImages[i].file = (pic as! PFFile)
                self.attendeeImages[i].loadInBackground()
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showOtherProfileSegue") {
            let dest = (segue.destinationViewController as! ProfileViewController)
            if (selectedAttendee == -1) {
                dest.user = (game[Event.Creator] as! PFUser)
            } else {
                dest.user = (attendees[selectedAttendee])
            }
            dest.enableBack = true
        }
    }
    
    @IBAction func joinGame(sender: AnyObject) {
        if (joinState == JoinButton.Join) {
            game.relationForKey(Event.Users).addObject(PFUser.currentUser()!)
            game.saveInBackgroundWithBlock() {
                (success, error) -> Void in
                if (success) {
                    self.loadAttendees()
                    self.joinState = JoinButton.Leave
                } else {
                    self.joinState = JoinButton.Join
                }
            }
            let gameRelation = PFUser.currentUser()?.relationForKey(User.Games)
            gameRelation?.addObject(game)
            PFUser.currentUser()!.saveInBackground()
            joinState = JoinButton.None
        } else if (joinState == JoinButton.Leave) {
            game.relationForKey(Event.Users).removeObject(PFUser.currentUser()!)
            game.saveInBackgroundWithBlock() {
                (success, error) -> Void in
                if (success) {
                    self.loadAttendees()
                    self.joinState = JoinButton.Join
                } else {
                    self.joinState = JoinButton.Leave
                }
            }
            let gameRelation = PFUser.currentUser()?.relationForKey(User.Games)
            gameRelation?.addObject(game)
            PFUser.currentUser()!.saveInBackground()
            joinState = JoinButton.None
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
