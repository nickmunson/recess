//
//  LoginViewController.swift
//  Recess
//
//  Created by Nick Munson on 4/13/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit
import Parse
import Bolts
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginContainer: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupContainer: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var facebookContainer: UIView!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    
    var activeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundCorners()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "checkLogin", name: UIApplicationDidBecomeActiveNotification, object: Optional.None)
        
        NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationDidBecomeActiveNotification,
            object: self,
            queue: nil,
            usingBlock: {(notification : NSNotification!) in
                self.checkLogin()
        })
        
        NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationDidFinishLaunchingNotification,
            object: self,
            queue: nil,
            usingBlock: {(notification : NSNotification!) in
                self.checkLogin()
        })
        
        NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationWillEnterForegroundNotification,
            object: self,
            queue: nil,
            usingBlock: {(notification : NSNotification!) in
                self.checkLogin()
        })
        
        NSNotificationCenter.defaultCenter().addObserverForName(UIApplicationWillResignActiveNotification,
            object: self,
            queue: nil,
            usingBlock: {(notification : NSNotification!) in
                self.checkLogin()
        })
        
        usernameField.delegate = self
        passwordField.delegate = self
        
        registerForKeyboardNotifications()
    }
    
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardHide:", name: UIKeyboardDidHideNotification, object: nil)
    }
    
    func keyboardShow(notification: NSNotification) {
        let info: NSDictionary = notification.userInfo!
        
        let kbSize: CGSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue().size
        
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0,0.0,kbSize.height,0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        
        // If active text field is hidden by keyboard, scroll it so it's visible

        var aRect: CGRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
            self.scrollView.scrollRectToVisible(activeTextField.frame, animated:true)
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextField = textField
    }
    
    func keyboardHide(notification: NSNotification){
        let contentInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func checkLogin() {
        let accessToken = FBSDKAccessToken.currentAccessToken()
        if (accessToken != Optional.None) {
            PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken) {
            (user: PFUser?, error: NSError?) -> Void in
                if (user != nil) {
//                    println("User logged in through Facebook!")
                    self.updateFacebookInfo()
                    self.performSegueWithIdentifier("loginSegue", sender: self)
                } else {
                    println("Uh oh. There was an error logging in.")
                }
            }
        } else if (PFUser.currentUser() != nil) {
            performSegueWithIdentifier("loginSegue", sender: self)
        }
//        println("checkLogin")
    }
    
    func roundCorners() {
        loginContainer.layer.cornerRadius = CORNER_RADIUS
        loginButton.layer.cornerRadius = CORNER_RADIUS
        signupContainer.layer.cornerRadius = CORNER_RADIUS
        signupButton.layer.cornerRadius = CORNER_RADIUS
        facebookContainer.layer.cornerRadius = CORNER_RADIUS
        facebookButton.layer.cornerRadius = CORNER_RADIUS
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if (identifier == "loginSegue" && PFUser.currentUser() == nil) {
            PFUser.logInWithUsernameInBackground(usernameField.text, password:passwordField.text) {
                (user: PFUser?, error: NSError?) -> Void in
                if user != nil {
                    PFQuery.clearAllCachedResults()
                    self.performSegueWithIdentifier("loginSegue", sender: self)
                } else {
                    let alert = UIAlertController(title: "Login Failed", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
                        return
                    }
                    
                    alert.addAction(defaultAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
            return false
        }
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidHideNotification, object: nil)
    }
    
    @IBAction func facebookLogin(sender: AnyObject) {
        PFFacebookUtils.logInInBackgroundWithReadPermissions(nil) {
        (user: PFUser?, error: NSError?) -> Void in
            if let user = user {
                if user.isNew {
                    self.updateFacebookInfo() // TODO set email to FB provided email
                    self.performSegueWithIdentifier("loginToWelcomeScreen", sender: self)
                } else {
                    println("User logged in through Facebook!")
                }
            } else {
                println("Uh oh. The user cancelled the Facebook login.")
            }
        }
    }

    func updateFacebookInfo() {
        let request = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        request.startWithCompletionHandler() {
            (connection, result, error) -> Void in
            if let error = error {
                
            } else {
                let userData = result as! NSDictionary
                let facebookID: AnyObject? = userData["id"]
                PFUser.currentUser()?[User.DisplayName] = userData["name"]
                PFUser.currentUser()?[User.Username] = facebookID
                let picURL = NSURL(string: NSString(format: "https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID! as! NSString) as String)
                let urlRequest = NSURLRequest(URL: picURL!)
                NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue.mainQueue(), completionHandler: { (response, data, error) -> Void in
                    if let error = error {
                        println(error)
                    } else {
                        let imageFile = PFFile(name: "profile.png", data: data!)
                        imageFile.saveInBackgroundWithBlock {
                            (success, error) -> Void in
                            if (success) {
                                let user = PFUser.currentUser()
                                user?[User.ProfilePicture] = imageFile
                                user?.saveInBackground()
                            } else {
                                let alert = UIAlertView()
                                alert.title = "Please try again later"
                                alert.message = "Check your internet connection"
                                alert.show()
                            }
                        }
                    }
                })
            }
        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
