//
//  ChoosePlayersNewGameViewController.swift
//  Recess
//
//  Created by Nick Munson on 5/1/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class ChoosePlayersNewGameViewController: UIViewController {
    
    @IBOutlet weak var backContainer: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextContainer: UIView!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var numPlayersTextField: UITextField!

    var game: PFObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundCorners()
    }
    
    func roundCorners() {
        backContainer.layer.cornerRadius = CORNER_RADIUS
        backButton.layer.cornerRadius = CORNER_RADIUS
        nextContainer.layer.cornerRadius = CORNER_RADIUS
        nextButton.layer.cornerRadius = CORNER_RADIUS
    }
    
    @IBAction func next(sender: AnyObject) {
        if let num: Int = numPlayersTextField.text?.toInt() {
            if (num >= 2) {
                game[Event.NumPlayers] = num
                performSegueWithIdentifier("confirmGameSegue", sender: self)
                return
            }
        }
        
        let alert = UIAlertController(title: "Please enter a number of players", message: "Must be at least two", preferredStyle: UIAlertControllerStyle.Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
            return
        }
        
        alert.addAction(defaultAction)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "confirmGameSegue") {
            (segue.destinationViewController as! ConfimNewGameViewController).game = game
        }
    }
    
    @IBAction func dismissKeyboard(sender: AnyObject) {
        numPlayersTextField.resignFirstResponder()
    }
    
    
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
