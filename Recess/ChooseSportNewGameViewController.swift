//
//  ChoseSportNewGameViewController.swift
//  Recess
//
//  Created by Nick Munson on 5/1/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class ChooseSportNewGameViewController: UIViewController {
    
    @IBOutlet weak var a: UIButton!
    @IBOutlet weak var b: UIButton!
    @IBOutlet weak var c: UIButton!
    @IBOutlet weak var d: UIButton!
    @IBOutlet weak var e: UIButton!
    @IBOutlet weak var f: UIButton!
    
    @IBOutlet weak var g: UIButton!
    @IBOutlet weak var h: UIButton!
    @IBOutlet weak var i: UIButton!
    @IBOutlet weak var j: UIButton!
    @IBOutlet weak var k: UIButton!
    @IBOutlet weak var l: UIButton!
    
    @IBOutlet weak var cancelContainer: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    var game: PFObject

    required init(coder aDecoder: NSCoder) {
        game = PFObject(className: "Event")
        game[Event.Creator] = PFUser.currentUser()
        var usersRelation = game.relationForKey(Event.Users)
        usersRelation.addObject(PFUser.currentUser()!)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundCorners()
    }
    
    func roundCorners() {
        a.layer.cornerRadius = CORNER_RADIUS
        b.layer.cornerRadius = CORNER_RADIUS
        c.layer.cornerRadius = CORNER_RADIUS
        d.layer.cornerRadius = CORNER_RADIUS
        e.layer.cornerRadius = CORNER_RADIUS
        f.layer.cornerRadius = CORNER_RADIUS
        
        g.layer.cornerRadius = CORNER_RADIUS
        h.layer.cornerRadius = CORNER_RADIUS
        i.layer.cornerRadius = CORNER_RADIUS
        j.layer.cornerRadius = CORNER_RADIUS
        k.layer.cornerRadius = CORNER_RADIUS
        l.layer.cornerRadius = CORNER_RADIUS
        
        cancelContainer.layer.cornerRadius = CORNER_RADIUS
        cancelButton.layer.cornerRadius = CORNER_RADIUS
    }
    
    func continueWithSport(sport: String) {
        game[Event.Sport] = sport
        performSegueWithIdentifier("choseLocationSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "choseLocationSegue") {
            (segue.destinationViewController as! ChooseLocationNewGameViewController).game = game
        }
    }
    
    @IBAction func choseBaseball(sender: AnyObject) {
        continueWithSport(Sport.Baseball)
    }
    
    @IBAction func choseBasketball(sender: AnyObject) {
        continueWithSport(Sport.Basketball)
    }
    
    @IBAction func choseFootball(sender: AnyObject) {
        continueWithSport(Sport.Football)
    }
    
    @IBAction func choseVolleyball(sender: AnyObject) {
        continueWithSport(Sport.Volleyball)
    }
    
    @IBAction func choseTenis(sender: AnyObject) {
        continueWithSport(Sport.Tennis)
    }
    
    @IBAction func choseSoccer(sender: AnyObject) {
        continueWithSport(Sport.Soccer)
    }
    
    @IBAction func choseRaquetball(sender: AnyObject) {
        continueWithSport(Sport.Raquetball)
    }
    
    @IBAction func choseSwimming(sender: AnyObject) {
        continueWithSport(Sport.Swimming)
    }
    
    @IBAction func choseBadminton(sender: AnyObject) {
        continueWithSport(Sport.Badminton)
    }
    
    @IBAction func choseHockey(sender: AnyObject) {
        continueWithSport(Sport.Hockey)
    }
    
    @IBAction func choseRunning(sender: AnyObject) {
        continueWithSport(Sport.Running)
    }
    
    @IBAction func choseFrisby(sender: AnyObject) {
        continueWithSport(Sport.Frisby)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
