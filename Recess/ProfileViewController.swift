//
//  ProfileViewController.swift
//  Recess
//
//  Created by Nick Munson on 4/10/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var profileImage: PFImageView!
    @IBOutlet weak var futureGamesLabel: UILabel!
    @IBOutlet weak var allGamesLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var favoriteSport1Image: UIImageView!
    @IBOutlet weak var favoriteSport2Image: UIImageView!
    @IBOutlet weak var favoriteSport3Image: UIImageView!
    
    @IBOutlet weak var backLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var newGameLabel: UILabel!
    @IBOutlet weak var newGameButton: UIButton!
    
    var games : Array<PFObject>? = Optional.None
    var selectedInd: Int = 0
    var user: PFUser
    var enableBack = false
    
    required init(coder aDecoder: NSCoder) {
        user = PFUser.currentUser()!
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateProfileImage()
        updateUsername()
        loadGames()
        roundCorners()
        setupTableView()
        checkBackButton()
    }
    
    func checkBackButton() {
        if (enableBack) {
            backButton.hidden = false
            backLabel.hidden = false
            newGameButton.hidden = true
            newGameLabel.hidden = true
        }
    }
    
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNib(UINib(nibName: "gameCell", bundle: nil), forCellReuseIdentifier: "gameCell")
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("gameCell")! as! MyGamesTableViewCell
        cell.game = games![indexPath.row as Int]
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let games = games {
            return count(games)
        } else {
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func roundCorners() {
        newGameButton.layer.cornerRadius = newGameButton.frame.width / 2.0
        backButton.layer.cornerRadius = newGameButton.frame.width / 2.0
        profileImage.layer.cornerRadius = CORNER_RADIUS
    }
    
    func updateProfileImage() {
        let pic: PFFile = user[User.ProfilePicture] as! PFFile
        profileImage.file = pic
        profileImage.loadInBackground()
    }
    
    func updateUsername() {
        nameLabel.text = (user[User.DisplayName] as! String)
    }

    func loadGames() {
        let relation = user.relationForKey(User.Games)
        
        var futureQuery = relation.query()
        futureQuery?.whereKey(Event.Time, greaterThan: NSDate())
        futureQuery!.findObjectsInBackgroundWithBlock() {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                if let objects = objects as? [PFObject] {
                    self.futureGamesLabel.text = count(objects).description
                }
            }
        }
        
        var pastQuery = relation.query()
        pastQuery?.whereKey(Event.Time, lessThanOrEqualTo: NSDate())
        pastQuery!.findObjectsInBackgroundWithBlock() {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                if let objects = objects as? [PFObject] {
                    self.allGamesLabel.text = count(objects).description
                    self.games = objects
                    self.tableView.reloadData()
                    var dict = [String: Int]()
                    for game in objects {
                        let key = game[Event.Sport] as! String
                        if let val = dict[key] {
                            dict.updateValue(val + 1, forKey: key)
                        } else {
                            dict[key] = 1
                        }
                    }
                    var most = ("", 0)
                    var second = ("", 0)
                    var third = ("", 0)
                    for (sport, count) in dict {
                        if (count >= most.1) {
                            third = second
                            second = most
                            most = (sport, count)
                        } else if (count >= second.1) {
                            third = second
                            second = (sport, count)
                        } else if (count >= third.1) {
                            third = (sport, count)
                        }
                    }
                    if (most.1 != 0) {
                        self.favoriteSport1Image.image = Sport.image(most.0)
                    }
                    if (second.1 != 0) {
                        self.favoriteSport2Image.image = Sport.image(second.0)
                    }
                    if (third.1 != 0) {
                        self.favoriteSport3Image.image = Sport.image(third.0)
                    }
                }
            }
        }
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedInd = indexPath.row
        performSegueWithIdentifier("showDetailProfileSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showDetailProfileSegue") {
            (segue.destinationViewController as! GameDetailViewController).game = games![selectedInd]
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    override func viewWillAppear(animated: Bool) {
        if let ind = tableView.indexPathForSelectedRow() {
            tableView.deselectRowAtIndexPath(ind, animated: false)
        }
        loadGames()
    }
    
}
