//
//  ConfimNewGameViewController.swift
//  Recess
//
//  Created by Nick Munson on 5/1/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class ConfimNewGameViewController: UIViewController {
    
    var game: PFObject!
    
    @IBOutlet weak var backContainer: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var confirmContainer: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var playersLabel: UILabel!
    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var creatorImage: PFImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playersLabel.text = (game[Event.NumPlayers] as! Int).description
        locationLabel.text = (game[Event.Location] as! String)
        let date: NSDate = (game["date"] as! NSDate)
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.LongStyle
        formatter.timeStyle = NSDateFormatterStyle.NoStyle
        dateLabel.text = formatter.stringFromDate(date)
        
        let time: NSDate = (game["date"] as! NSDate)
        let formatter2 = NSDateFormatter()
        formatter2.dateStyle = NSDateFormatterStyle.NoStyle
        formatter2.timeStyle = NSDateFormatterStyle.ShortStyle
        timeLabel.text = formatter2.stringFromDate(time)
        sportImage.image = Sport.image(game["sport"] as! String)
        creatorImage.file = (PFUser.currentUser()!["profilePicture"] as! PFFile)
        creatorImage.loadInBackground()
        roundCorners()
    }
    
    func roundCorners() {
        backContainer.layer.cornerRadius = CORNER_RADIUS
        backButton.layer.cornerRadius = CORNER_RADIUS
        confirmContainer.layer.cornerRadius = CORNER_RADIUS
        confirmButton.layer.cornerRadius = CORNER_RADIUS
        sportImage.layer.cornerRadius = CORNER_RADIUS
    }

    
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        creatorImage.layer.cornerRadius = creatorImage.frame.width / 2.0
    }

    // MARK: - Navigation

    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if (identifier == "returnNewGameSegue") {
            game.saveInBackgroundWithBlock({ (success, error) -> Void in
                let gamesRelation = PFUser.currentUser()?.relationForKey("games")
                gamesRelation?.addObject(self.game)
                PFUser.currentUser()?.saveInBackgroundWithBlock() {
                    (success, error) -> Void in
                    self.performSegueWithIdentifier("returnNewGameSegue", sender: self)
                }
            })
        }
        return false
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
