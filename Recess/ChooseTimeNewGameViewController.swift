//
//  ChooseTimeNewGameViewController.swift
//  Recess
//
//  Created by Nick Munson on 5/1/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class ChooseTimeNewGameViewController: UIViewController {
    
    @IBOutlet weak var backContainer: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextContainer: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var game: PFObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        roundCorners()
        datePicker.minimumDate = NSDate()
    }
    
    func roundCorners() {
        backContainer.layer.cornerRadius = CORNER_RADIUS
        backButton.layer.cornerRadius = CORNER_RADIUS
        nextContainer.layer.cornerRadius = CORNER_RADIUS
        nextButton.layer.cornerRadius = CORNER_RADIUS
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "choosePlayersSegue") {
            (segue.destinationViewController as! ChoosePlayersNewGameViewController).game = game
        }
    }
    
    @IBAction func next(sender: AnyObject) {
        game[Event.Time] = datePicker.date
        performSegueWithIdentifier("choosePlayersSegue", sender: self)
    }
    
    @IBAction func back(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
