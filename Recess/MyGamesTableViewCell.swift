//
//  MyGamesTableViewCell.swift
//  Recess
//
//  Created by Nick Munson on 4/22/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit
import Parse
import Bolts
import FBSDKCoreKit

class MyGamesTableViewCell: UITableViewCell {

    @IBOutlet weak var sportImage: UIImageView!
    @IBOutlet weak var creatorImage: PFImageView!
    @IBOutlet weak var attendee1Image: PFImageView!
    @IBOutlet weak var attendee2Image: PFImageView!
    @IBOutlet weak var attendee3Image: PFImageView!
    @IBOutlet weak var attendee4Image: PFImageView!
    @IBOutlet weak var attendee5Image: PFImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var attendeeImages: Array<PFImageView> = []

    var _game : PFObject? = Optional.None
    var game: PFObject?
    {
        set {
            _game = newValue
            if (newValue != nil) {
                updateCell()
            }
        }
        get {
            return _game
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        creatorImage.layer.cornerRadius = creatorImage.frame.width / 2.0
        attendeeImages.append(attendee1Image)
        attendeeImages.append(attendee2Image)
        attendeeImages.append(attendee3Image)
        attendeeImages.append(attendee4Image)
        attendeeImages.append(attendee5Image)
        roundAttendees()
    }
    
    func roundAttendees() {
        for attendeeImage in attendeeImages {
            attendeeImage.layer.cornerRadius = attendeeImage.frame.width / 2.0
        }
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadAttendees() {
        for attendeeImage in attendeeImages {
            attendeeImage.file = nil
            attendeeImage.image = nil
        }
        let relation = game?.relationForKey("users")
        let query = relation?.query()
        query?.cachePolicy = PFCachePolicy.CacheThenNetwork
        query?.findObjectsInBackgroundWithBlock() {
            (result, error) -> Void in
            if let attendees = result {
                if (count(attendees) > 0) {
                    for i in 0..<count(attendees)  {
                        if let pic: AnyObject = attendees[i]["profilePicture"] {
                            self.attendeeImages[i].file = (pic as! PFFile)
                            self.attendeeImages[i].loadInBackground()
                        }
                    }
                }
            }
        }
    }

    func updateCell() {
        dateLabel.text = Event.gameDate(game)
        timeLabel.text = Event.gameTime(game)
        sportImage.image = Sport.image(game!["sport"] as! String)
        creatorImage.image = UIImage(contentsOfFile: "Person-icon-grey.jpg")
        
        loadAttendees()

        let creator: PFObject = (game!["creator"] as! PFObject)
        creator.fetchIfNeededInBackgroundWithBlock() {
            (object, error) -> Void in
            if let error = error {
                println(error)
            } else {
                if let pic: AnyObject = creator["profilePicture"] {
                    self.creatorImage.file = (pic as! PFFile)
                    self.creatorImage.layer.cornerRadius = self.creatorImage.frame.width / 2.0
                    self.creatorImage.loadInBackground() {
                        (image, error) -> Void in
                        self.creatorImage.layer.cornerRadius = self.creatorImage.frame.width / 2.0
                    }
                }
            }
        }
    }

}
