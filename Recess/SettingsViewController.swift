//
//  SettingsViewController.swift
//  Recess
//
//  Created by Nick Munson on 4/10/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var logoutContainer: UIView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var newGameButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        roundCorners()
    }
    
    func roundCorners() {
        newGameButton.layer.cornerRadius = newGameButton.frame.width / 2.0
        logoutContainer.layer.cornerRadius = CORNER_RADIUS
        logoutButton.layer.cornerRadius = CORNER_RADIUS
    }

    @IBAction func logout(sender: AnyObject) {
        PFUser.logOut()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
