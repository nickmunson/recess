//
//  CreateProfileIntroViewController.swift
//  Recess
//
//  Created by Nick Munson on 4/28/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class CreateProfileIntroViewController: UIViewController {

    @IBOutlet weak var profilePictureImage: PFImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var beginContainer: UIView!
    @IBOutlet weak var beginButton: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }
    
    override func viewWillAppear(animated: Bool) {
        updateUsername()
        updateProfileImage()
        roundCorners()
    }
    
    func roundCorners() {
        beginContainer.layer.cornerRadius = CORNER_RADIUS
        beginButton.layer.cornerRadius = CORNER_RADIUS
    }
    
    func updateUsername() {
        let user = PFUser.currentUser()!
        let name = (user[User.DisplayName] as! String)
        welcomeLabel.text = "Welcome \(name)!"
    }
    
    func updateProfileImage() {
        let user = PFUser.currentUser()!
        let pic: PFFile = user["profilePicture"] as! PFFile
        profilePictureImage.file = pic
        profilePictureImage.loadInBackground()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
