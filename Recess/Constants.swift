//
//  Constants.swift
//  Recess
//
//  Created by Nick Munson on 4/14/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import Foundation
import CoreGraphics

let CORNER_RADIUS : CGFloat = 4.0

struct User {
    static let DisplayName = "displayName"
    static let Email = "email"
    static let ProfilePicture = "profilePicture"
    static let Games = "games"
    static let Username = "username"
}

struct Sport
{
    static let All = "All"
    static let Baseball = "Baseball"
    static let Basketball = "Basketball"
    static let Volleyball = "Volleyball"
    static let Hockey = "Hockey"
    static let Raquetball = "Raquetball"
    static let Tennis = "Tennis"
    static let Football = "Football"
    static let Soccer = "Soccer"
    static let Swimming = "Swimming"
    static let Badminton = "Badmintton"
    static let Running = "Running"
    static let Frisby = "Frisbee"
    
    static func image(name: String) -> UIImage {
        if let image = UIImage(named: name, inBundle: nil, compatibleWithTraitCollection: nil) {
            return image
        } else {
            println("Could not find image called " + name)
            return UIImage(named: "Person-icon-grey.jpg", inBundle: nil, compatibleWithTraitCollection: nil)!
        }
    }
}

struct Venue
{
    static let Wiegand = "Wiegand"
    static let SkiboMain = "Skibo Main"
    static let SkiboArena = "Skibo Arena"
    static let SwimmingPool = "Swimming Pool"
    static let GeslingTrack = "Gesling Track"
    static let GeslingField = "Gesling Field"
    static let TenisCourts = "Tenis Courts"
    static let IntramuralField = "Intramural Field"
    static let RaquetballCourts = "Raquetball Courts"
    static let TheCut = "The Cut"
    static let CFALawn = "CFA Lawn"
}

struct Event {
    static let NumPlayers = "numPlayers"
    static let Location = "location"
    static let Creator = "creator"
    static let Time = "date"
    static let Sport = "sport"
    static let Users = "users"
    
    static func location(game: PFObject) -> String {
        return (game[Event.Location] as! String)
    }
    
    static func numPlayers(game: PFObject) -> String {
        return (game[Event.NumPlayers] as! Int).description
    }
    
    static func formatDate(game: PFObject) -> String {
        let date: NSDate = (game["date"] as! NSDate)
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.LongStyle
        formatter.timeStyle = NSDateFormatterStyle.NoStyle
        return formatter.stringFromDate(date)
    }
    
    static func formatTime(game: PFObject) -> String {
        let date: NSDate = (game["date"] as! NSDate)
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.NoStyle
        formatter.timeStyle = NSDateFormatterStyle.ShortStyle
        return formatter.stringFromDate(date)
    }
    
    
    static func gameDate(game: PFObject?) -> String {
        if let game = game {
            let date: NSDate = (game["date"] as! NSDate)
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.LongStyle
            formatter.timeStyle = NSDateFormatterStyle.NoStyle
            return formatter.stringFromDate(date)
        }
        return ""
    }
    
    static func gameTime(game: PFObject?) -> String {
        if let game = game {
            let date: NSDate = (game["date"] as! NSDate)
            let formatter = NSDateFormatter()
            formatter.dateStyle = NSDateFormatterStyle.NoStyle
            formatter.timeStyle = NSDateFormatterStyle.ShortStyle
            return formatter.stringFromDate(date)
        }
        return ""
    }
}