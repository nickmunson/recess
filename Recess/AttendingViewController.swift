//
//  AttendingViewController.swift
//  Recess
//
//  Created by Nick Munson on 4/10/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit
import Parse
import Bolts
import FBSDKCoreKit

class AttendingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var helpText: UILabel!
    @IBOutlet weak var newGameButton: UIButton!
    
    var games : Array<PFObject>? = Optional.None
    var selectedInd = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerNib(UINib(nibName: "gameCell", bundle: nil), forCellReuseIdentifier: "myGamesGameCell")
        loadGames()
        roundNewGameButton()
    }
    
    func roundNewGameButton() {
        newGameButton.layer.cornerRadius = newGameButton.frame.width / 2.0
    }
    
    override func viewWillAppear(animated: Bool) {
        if let ind = tableView.indexPathForSelectedRow() {
            tableView.deselectRowAtIndexPath(ind, animated: false)
        }
        
        loadGames()
    }

    func loadGames() {
        var currentUserOp = PFUser.currentUser()
        var query = PFUser.currentUser()?.relationForKey(User.Games).query()
        query?.whereKey(Event.Time, greaterThan: NSDate(timeIntervalSinceNow: NSTimeInterval(-60*60)))
        query!.findObjectsInBackgroundWithBlock() {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                if let objects = objects as? [PFObject] {
                    self.games = objects as Array<PFObject>
                    self.games?.sort({ (game1, game2) -> Bool in
                        return (game1[Event.Time] as! NSDate).compare(game2[Event.Time] as! NSDate) == NSComparisonResult.OrderedAscending
                    })
                    self.tableView.reloadData()
                }
            } else {
                // Log details of the failure
                //                println("Error: \(error) \(error.userInfo!)")
            }
        }
        if let currentUser = currentUserOp {
            /// PFUser.currentUser()!["events"] as? Array<PFObject>
        } else {
            println("Error, no one logged in")
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myGamesGameCell")! as! MyGamesTableViewCell
        cell.game = games![indexPath.row as Int]
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let g = games {
            if (count(g) > 0) {
                tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
                helpText.hidden = true
                return count(g)
            }
        }
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        helpText.hidden = false
        return 0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedInd = indexPath.row
        performSegueWithIdentifier("showDetailAttendingSegue", sender: self)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showDetailAttendingSegue") {
            (segue.destinationViewController as! GameDetailViewController).game = games![selectedInd]
        }
    }

}
