//
//  FindGamesViewController.swift
//  Recess
//
//  Created by Nick Munson on 4/10/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class FindGamesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newGameButton: UIButton!
    @IBOutlet weak var helpText: UILabel!
    
    var sport: String = Sport.All
    var games : Array<PFObject>? = Optional.None
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNib(UINib(nibName: "gameCell", bundle: nil), forCellReuseIdentifier: "gameCell")
        
        loadGames()
        roundNewGameButton()
    }

    func roundNewGameButton() {
        newGameButton.layer.cornerRadius = newGameButton.frame.width / 2.0
    }
    
    func loadGames() {
        var currentUserOp = PFUser.currentUser()
        var query = PFQuery(className:"Event")
        query.whereKey("date", greaterThan: NSDate(timeIntervalSinceNow: NSTimeInterval(-60*60)))
        if (sport != Sport.All) {
            query.whereKey("sport", equalTo: sport).description
        }
        query.findObjectsInBackgroundWithBlock() {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil {
                if let objects = objects as? [PFObject] {
                    self.games = objects as Array<PFObject>
                    self.games?.sort({ (game1, game2) -> Bool in
                        return (game1["date"] as! NSDate).compare(game2["date"] as! NSDate) == NSComparisonResult.OrderedAscending
                    })
                    self.tableView.reloadData()
                }
            } else {
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if let ind = tableView.indexPathForSelectedRow() {
            tableView.deselectRowAtIndexPath(ind, animated: false)
        }
        loadGames()
    }
    
    @IBAction func showAllSports(sender: AnyObject) {
        sport = Sport.All
        loadGames()
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("gameCell")! as! MyGamesTableViewCell
        cell.game = games![indexPath.row as Int]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedIndex = indexPath.row
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        performSegueWithIdentifier("showDetailFromBulletinSegue", sender: self)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90.0
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let g = games {
            if (count(g) > 0) {
                tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
                helpText.hidden = true
                return count(g)
            }
        }
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        helpText.hidden = false
        return 0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showDetailFromBulletinSegue") {
            (segue.destinationViewController as! GameDetailViewController).game = games![selectedIndex]
        }
        if (segue.identifier == "filterSportSegue") {
            (segue.destinationViewController as! FilterSportViewController).delegate = self
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
