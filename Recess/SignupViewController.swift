//
//  SignupViewController.swift
//  Recess
//
//  Created by Nick Munson on 4/14/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit
import ParseUI

class SignupViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var signupContainer: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var cancelContainer: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var profileImage: PFImageView!
    
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    var imageView: UIImageView!
    var imagePicker: UIImagePickerController?
    var user: PFUser?
    var activeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundCorners()
        user = PFUser()
        let image: UIImage = UIImage(named: "Person-icon-grey.jpg")!
        userNameField.delegate = self
        passwordField.delegate = self
        registerForKeyboardNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: Optional.None)
    }
    
    @IBAction func pickImage(sender: AnyObject) {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)) {
            if (imagePicker == Optional.None) {
                imagePicker = UIImagePickerController()
                imagePicker?.allowsEditing = true
                imagePicker?.delegate = self
            }
            presentViewController(imagePicker!, animated: true, completion: nil)
        } else {
            let alert = UIAlertView()
            alert.title = "Can not select profile photo"
            alert.message = "Photo Library not available"
            alert.show()
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        // Get the chosen image
        var image = info[UIImagePickerControllerEditedImage] as! UIImage
        
        // Check Dimensions
        let imageSize: CGSize = image.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        
        let minHeight: CGFloat = 100.0
        let minWidth: CGFloat = minHeight
        
        if (height < minHeight || width < minWidth) {
            let alert = UIAlertView()
            alert.title = "Image too small"
            alert.message = "Image must be at least \(minHeight) by \(minWidth) pixels"
            alert.show()
            self.dismissViewControllerAnimated(true, completion: nil)
            return
        }
        
        // Crop the image to a square
        if (width != height) {
            let newDimension: CGFloat = min(width, height)
            let widthOffset: CGFloat = (width - newDimension) / 2
            let heightOffset: CGFloat = (height - newDimension) / 2
            
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), false, 0.0)
            image.drawAtPoint(CGPointMake(-widthOffset, -heightOffset),
                blendMode:kCGBlendModeCopy,
                alpha:1.0)
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        // Show the image
        profileImage.image = image
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardShow:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardHide:", name: UIKeyboardDidHideNotification, object: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if (identifier == "signupNewUser") {
            signupButton.enabled = false
            signup()
        }
        return false
    }
    
    
    func keyboardShow(notification: NSNotification) {
        let info: NSDictionary = notification.userInfo!
        
        let kbSize: CGSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue().size
        
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0,0.0,kbSize.height,0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        
        var aRect: CGRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeTextField.frame.origin) ) {
            self.scrollView.scrollRectToVisible(activeTextField.frame, animated:true)
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextField = textField
    }
    
    func keyboardHide(notification: NSNotification){
        let contentInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func signup() {
        // TODO - display waiting indicator
        
        // Verify and save email and password
        user?[User.DisplayName] = userNameField.text
        user?.username = emailField.text
        user?.password = passwordField.text
        user?.email = emailField.text
        
        // Upload Image
        let imageData: NSData = UIImagePNGRepresentation(profileImage.image)
        let imageFile = PFFile(name: "profile.png", data: imageData)
        imageFile.saveInBackgroundWithBlock { (success, error) -> Void in
            if (success) {
                self.user?["profilePicture"] = imageFile
                self.user?.signUpInBackgroundWithBlock({ (success, error) -> Void in
                    if let error = error {
                        let errorString = error.userInfo?["error"] as? NSString
                        let alert = UIAlertController(title: "There was a problem signing up", message: (errorString as! String), preferredStyle: UIAlertControllerStyle.Alert)
                        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
                            return
                        }
                        alert.addAction(defaultAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                        
                        self.signupButton.enabled = true
                    } else {
                        self.performSegueWithIdentifier("signupNewUser", sender: self)
                    }
                })
            } else {
                let alert = UIAlertView()
                alert.title = "Please try again later"
                alert.message = "Check your internet connection"
                alert.show()
                self.signupButton.enabled = true
            }
        }

    }
    
    func roundCorners() {
        signupContainer.layer.cornerRadius = CORNER_RADIUS
        signupButton.layer.cornerRadius = CORNER_RADIUS
        cancelContainer.layer.cornerRadius = CORNER_RADIUS
        cancelButton.layer.cornerRadius = CORNER_RADIUS
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
