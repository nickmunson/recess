//
//  MainTabBarController.swift
//  Recess
//
//  Created by Nick Munson on 5/3/15.
//  Copyright (c) 2015 Nick Munson. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        for tabBarItem in tabBar.items! {
            let tab = (tabBarItem as! UITabBarItem)
            tab.image = tab.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)

            UITabBarItem.appearance().setTitleTextAttributes(
                NSDictionary(objects: [UIColor.whiteColor()],
                             forKeys: [NSForegroundColorAttributeName]) as [NSObject : AnyObject],
                forState: UIControlState.Normal)
            
            let color = UIColor(red: (255.0/255.0), green: (196.0/255.0), blue: (46.0/255.0), alpha: 1.0)
            
            let yellowDict = Dictionary<NSObject,AnyObject>(dictionaryLiteral: (NSForegroundColorAttributeName, color))
            
            UITabBarItem.appearance().setTitleTextAttributes(
                yellowDict,
                forState: UIControlState.Selected)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
